#include "ImageScrambler.h"
#include "ui_ImageScrambler.h"


/// \fn ImageScrambler::ImageScrambler(QWidget *parent)
/// \~English
/// \brief Constructor
/// \~Spanish
/// \brief Constructor
ImageScrambler::ImageScrambler(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ImageScrambler)
{
    ui->setupUi(this);

    scrambleDepth = 1 ;

    // KEYBOARD SHORTCUTS
    // ATAJOS DEL KEYBOARD

    // Loads an image with cmd + o
    // Carga una imagen con cmd + o
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this, SLOT(on_btnLoadNewImage_clicked()));
    // Saves an image with cmd + s
    // Guarda una imagen con cmd + s
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this, SLOT(on_btnSave_clicked()));
    // Closes window with cmd + w
    // Cierra la ventana con cmd + w
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_W), this, SLOT(close()));
    // Scrambles an image with cmd + e
    // Revuelve una imagen con cmd + e
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_E), this, SLOT(on_btnScrambleImage_clicked()));
    // Restores the image with cmd + d
    // Restaura la imagen con cmd + d
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_D), this, SLOT(on_btnDescrambleImage_clicked()));
    ui->btnDescrambleImage->setEnabled(false);

}

/// \fn ImageScrambler::~ImageScrambler(QWidget *parent)
/// \~English
/// \brief Destructor
/// \~Spanish
/// \brief Destructor
ImageScrambler::~ImageScrambler()
{
    delete ui;
}

/// \fn void ImageScrambler::on_btnLoadNewImage_clicked()
/// \~English
/// \brief Function that displays a browsing window file to load an image into the GUI
/// \~Spanish
/// \brief Funcion que despliega una ventana de busqueda de archivos para cargar
/// una imagen en el GUI.
void ImageScrambler::on_btnLoadNewImage_clicked(){
    QString fname = QFileDialog::getOpenFileName(this, tr("Choose an image"), QDir::homePath());
        if (!fname.isEmpty()){
            QImage image(fname);
            if (image.isNull())
                QMessageBox::information(this, tr("Choose an image"),tr("Cannot load %1.").arg(fname));
            originalImage=image;
            scrambledImage=image;
            ui->lblOriginalImage->setPixmap(QPixmap::fromImage(originalImage));
            ui->lblScrambleImage->setPixmap(QPixmap::fromImage(scrambledImage));
            ui->btnDescrambleImage->setEnabled(false);
        }
}

/// \fn void ImageScrambler::on_btnSave_clicked()
/// \~English
/// \brief Function that saves the scrambled image in the computer.
/// \~Spanish
/// \brief Funcion que guarda la imagen revuelta en la computadora.
void ImageScrambler::on_btnSave_clicked(){
    QPixmap out = QPixmap::grabWidget(this,361,10,481,481);
    QString fname = QFileDialog::getSaveFileName(this, tr("Save Edited Image"), (""), tr("PNG (*.png)" ));
    scrambledImage.save(fname, "PNG");
}


/// \fn void ImageScrambler::on_btnScrambleImage_clicked()
/// \~English
/// \brief Function invokes the ScrambleFilter function to scramble the image and
/// load it in the GUI.
/// \~Spanish
/// \brief Funcion que invoca la funcion ScrambleFilter para revolver la imagen y
/// cargarla en el GUI.
void ImageScrambler::on_btnScrambleImage_clicked(){

    //Call Scramble to scramble the image
    //Llamar Scramble para revolver la imagen
    scrambleDepth = ui->scrambleDepth->value() ;
    scrambledImage = ScrambleFilter(originalImage, scrambleDepth, 0, 0, originalImage.width(),originalImage.height());
    ui->lblScrambleImage->setPixmap(QPixmap::fromImage(scrambledImage));
    ui->btnDescrambleImage->setEnabled(true);

}

/// \fn void ImageScrambler::on_btnDescrambleImage_clicked()
/// \~English
/// \brief Function that restores the scrambled image to the original image.
/// \~Spanish
/// \brief Funcion que restaura la imagen revuelta a la imagen original.
void ImageScrambler::on_btnDescrambleImage_clicked(){
    scrambledImage = originalImage;
    ui->lblScrambleImage->setPixmap(QPixmap::fromImage(scrambledImage));
}

/// \fn void ImageScrambler::on_actionLoad_Image_triggered()
/// \~English
/// \brief Function that invokes the on_btnLoadNewImage_clicked() function.
/// \~Spanish
/// \brief Funcion que invoca la funcion on_btnLoadNewImage_clicked().
void ImageScrambler::on_actionLoad_Image_triggered(){
    on_btnLoadNewImage_clicked();
}

/// \fn void ImageScrambler::on_actionSave_Image_triggered()
/// \~English
/// \brief Function that invokes the on_btnSave_clicked() function.
/// \~Spanish
/// \brief Funcion que invoca la funcion on_btnSave_clicked().
void ImageScrambler::on_actionSave_Image_triggered(){
    on_btnSave_clicked();
}

/// \fn void ImageScrambler::on_actionScramble_Image_triggered()
/// \~English
/// \brief Function that invokes the on_btnScrambleImage_clicked() function.
/// \~Spanish
/// \brief Funcion que invoca la funcion on_btnScrambleImage_clicked().
void ImageScrambler::on_actionScramble_Image_triggered(){
    on_btnScrambleImage_clicked();
}

/// \fn void ImageScrambler::on_actionDescramble_Image_triggered()
/// \~English
/// \brief Function that invokes the on_btnDescrambleImage_clicked() function.
/// \~Spanish
/// \brief Funcion que invoca la funcion on_btnDescrambleImage_clicked().
void ImageScrambler::on_actionDescramble_Image_triggered(){
    on_btnDescrambleImage_clicked();
}


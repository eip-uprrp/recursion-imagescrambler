#include "ImageScrambler.h"
#include <qdebug.h>


// Given an image and two rectangles defined by the x,y locations of their
// upperleft pixels and their height and width, the function swaps the pixels in those rectangles.
// For example, for an image I with height and width of 100. The following invocation
// swaps the top and bottom halves of the image:
// cropSwap(I, 0, 0, 0, 50, 100, 50)

void ImageScrambler::cropSwap(QImage &img, int x0, int y0, int x1, int y1, int width, int height ) {
    QRgb p;

    // For each of the pixels of the square with starting coord (x0,y0)
    // Por cada pixel del cuadrado con coordenada inicial (x0,y0)
    for (int dx = 0; dx < width; dx++) {
        for (int dy = 0; dy < height; dy++) {
            // extract the pixel
            // extrae el pixel
            p = img.pixel(x0 + dx , y0 + dy);
            // swap the pixel with the square starting coord (x1,y1)
            // intercabia el pixel con el cuadrado con coodenada inicial (x1,y1)
            img.setPixel(x0 + dx , y0 + dy, img.pixel(x1 + dx , y1 + dy));
            img.setPixel(x1 + dx , y1 + dy, p);
        }
    }
}

// This is the function you will code. Given the image, the level of recursion, 
// the x, y, width and height of the topleft corner of the rectangle that you'd 
// like to scramble, this function scrambles that part of the image.
// For example, for an image of height and width 100, to scramble the whole image
// two levels, you would invoke:
// ScrambleFilter(I, 2, 0, 0, 100, 100); 

QImage ImageScrambler::ScrambleFilter(QImage image, int level, int sx, int sy, int width, int height){
    
    return image;
}


